<div class="row product-box justify-content-between pt-5 pb-5">
    <?php foreach (getProducts() as $product) : ?>
    <div class="col-xl-4 col-lg-4 col-md-4 d-flex justify-content-between flex-column text-center mt-5 product-box-item">
        <h3><?= $product['heading'] ?></h3>

        <button class="btn btn-outline-primary d-flex mx-auto"
                data-toggle="modal"
                data-target="#products_modal"
                onclick="makeProductsContent(<?= $product['id'] ?>)">Хочу это</button>
    </div>
    <?php endforeach; ?>
</div>

<div class="modal fade" id="products_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content text-center">
            <div id="products_modal_body" class="modal-body">
                <h3></h3>
                <p>от <span id="span_product_price" class="font-weight-bold"> тыс.</span></p>
                <div class="content-list"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="footer__above-hr">
            <div class="footer__above-hr__main-text">
                <h2>Интересна работа в нашей команде?</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
            </div>
            <div class="footer__above-hr__button">
                <button class="footer__popup-button" data-toggle="modal" data-target="#contact_us_modal">Let’s Talk</button>
            </div>
        </div>
    </div>

    <hr>

    <div class="container">
        <div class="footer__under-hr">
            <ul class="footer--ul">
                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                <li><span>8&nbsp;904 535-26-28</span></li>
                <li><span>Damon3453@yandex.ru</span></li>
            </ul>
            <div class="footer__copyright-block text-left">
                <p><?= $currentYear ?> &copy; Все права защищены</p>
            </div>
        </div>
    </div>
</footer>

<header class="header">
    <ul class="d-flex justify-content-between align-items-center flex-row">
        <?php foreach ($headerMenuItems as $menuItem): ?>
        <?= $menuItem ?>
        <?php endforeach; ?>
    </ul>
</header>

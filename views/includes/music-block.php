<p>Послушайте музыку, пока проверяете задание</p>
<div id="musicCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <iframe src="https://music.yandex.ru/iframe/#track/53340723/7619409"></iframe>
        </div>
        <div class="carousel-item">
            <iframe src="https://music.yandex.ru/iframe/#track/38886531/4998570"></iframe>
        </div>
        <div class="carousel-item">
            <iframe src="https://music.yandex.ru/iframe/#track/2834228/305306"></iframe>
        </div>
        <div class="carousel-item">
            <iframe src="https://music.yandex.ru/iframe/#track/17301595/4811970"></iframe>
        </div>
        <div class="carousel-item">
            <iframe src="https://music.yandex.ru/iframe/#track/25130444/2955790"></iframe>
        </div>
    </div>
    <a class="carousel-control-prev" href="#musicCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#musicCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
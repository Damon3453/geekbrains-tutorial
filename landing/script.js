($(function () {
  getAllReviews()
}))

function increaseViewCount(id) {
  $.ajax({
    url: `../controllers/5-lesson.php`,
    method: 'get',
    data: {
      query: true,
      image_id: id
    },
    dataType: 'json',
    success: function (response) {
      $(`#views_count_${id}`).html(response.data.count)
      if (response.status === 201) {
          remakeGalleryHtml()
      }
    },
    error: function (error) {
      throw error
    }
  })
}

function remakeGalleryHtml() {
  $.ajax({
    url: `../controllers/5-lesson.php`,
    method: 'get',
    data: {
      remake: true,
    },
    dataType: 'json',
    success: function (response) {
      $('#db_gallery_container').html('')
      response.data.forEach(item => {
        $('#db_gallery_container').append(`
          <div class="col-md-4">
                <div class="row">
                    <a data-fancybox="gallery" href="..${item.path}/${item.name}" rel="noopener noreferrer">
                        <img onclick="increaseViewCount(${item.id})" src="..${item.path}/${item.name}" class="img-thumbnail" alt="collection-image" width="200" height="200">
                    </a>
                </div>
                <div class="row">
                    <span id="views_count_${item.id}"
                          class="badge rounded-pill bg-primary text-center w-50 mx-auto mb-3">
                        ${item.views}
                    </span>
                </div>
            </div>
        `)
      })
    },
    error: function (error) {
      throw error
    }
  })
}

$('#select_form').submit(function (e) {
  e.preventDefault()

  $.ajax({
    url: `../controllers/6-lesson.php`,
    method: 'post',
    data: {
      select_form: true,
      data: $(this).serializeArray()
    },
    dataType: 'json',
    success: function (response) {
      $('#select_result').html(response.data.result)
    },
    error: function (error) {
      throw error
    }
  })
})

$('.btn-check').click(function () {
  $('button[form=radio_form]').removeAttr('disabled')
})
// $('.btn-check').change(function () {
  $('#radio_form').submit(function (e) {
    e.preventDefault();

    $.ajax({
      url: `../controllers/6-lesson.php`,
      method: 'post',
      data: {
        radio_form: true,
        data: $(this).serializeArray()
      },
      dataType: 'json',
      success: function (response) {
        $('#radio_result').html(response.data.result)
      },
      error: function (error) {
        throw error
      }
    })
  })
// })

$('#reviews_form').submit(function (e) {
  e.preventDefault()
  $.ajax({
    url: `../controllers/6-lesson.php`,
    method: 'post',
    data: {
      add_review: true,
      data: {
        name: $('#user_name').val(),
        text: $('#text').val()
      }
    },
    dataType: 'json',
    success: function (response) {
      makeReviewsBlockContent(response)
    },
    error: function (error) {
      throw error
    }
  })
})

function getAllReviews() {
  $.ajax({
    url: `../controllers/6-lesson.php`,
    method: 'post',
    data: {
      get_all: true,
    },
    dataType: 'json',
    success: function (response) {
      makeReviewsBlockContent(response)
    },
    error: function (error) {
      throw error
    }
  })
}

function makeReviewsBlockContent(response) {
  $('#reviews-list').html('')
  $('#reviews-scrollspy').html('')
  response.data.forEach(item => {
    $('#reviews-list').append(
      `<a class="list-group-item list-group-item-action" href="#list-item-${item.id}">${item.name}</a>`
    )
    $('#reviews-scrollspy').append(
      `<div class="mb-3 d-flex flex-column justify-content-between border-bottom border-success review-block">` +
        `<h4 id="list-item-${item.id}">${item.name}</h4>` +
        `<p>${item.text}</p>` +
        `<small>${item.created_at}</small>` +
      `</div>`
    )
  })
}

function makeProductsContent(product_id) {
  $.ajax({
    url: `../controllers/6-lesson.php`,
    method: 'post',
    data: {
      get_products: true,
      product_id: product_id
    },
    dataType: 'json',
    success: function (response) {
      $('#products_modal_body > h3').html(`${response.heading}`)
      $('#span_product_price').html(`${response.price} тыс.`)
      $('.content-list').html(`${response.content}`)
    },
    error: function (error) {
      throw error
    }
  })
}
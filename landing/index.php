<?php
    require_once "../required_params.php";

    require_once '../controllers/MainController.php';
    require_once '../controllers/1-2-lessons.php';
    require_once '../controllers/3-lesson.php';
    require_once '../controllers/4-lesson.php';
    require_once '../controllers/5-lesson.php';
    require_once '../controllers/6-lesson.php';
    require_once '../controllers/7-lesson.php';
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Landing</title>

    <link rel="shortcut icon" href="../public/img/fav.ico">

    <link rel="stylesheet" href="../public/css/bootstrap5.min.css">
    <link rel="stylesheet" href="../public/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="btn btn-outline-primary no-fa mr-2" href="/"
                   data-toggle="tooltip"
                   data-placement="bottom"
                   title="Назад к вёрстке">
                    <i class="far fa-arrow-alt-circle-left no-fa"></i>
                </a>
                <a class="navbar-brand" href="#">Разработка сайтов (условно)</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#banner">Banner</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#products">Products</a>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-success" href="#homework">Якорь домашки</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <section id="banner">
        <div class="main-banner">
            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="laravel_logo.jpg" class="d-block w-100" alt="lara">
                    </div>
                    <div class="carousel-item">
                        <img src="php_logo.png" class="d-block w-100" alt="lara">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>

    <section id="products">
        <div class="container products pt-6">
            <h1 class="text-center">Цены на услуги, допустим</h1>
            <div class="row product-box justify-content-between pt-5 pb-5">
                <div class="col-xl-4 col-lg-4 col-md-4 d-flex justify-content-between flex-column text-center mt-5 product-box-item">
                    <h3>Лендинг</h3>
                    <p>от <span class="font-weight-bold">20 тыс.</span></p>
                    <ul>
                        <li>Одна страница</li>
                        <li>Размещение 1-2 услуг</li>
                        <li>Минимум информации</li>
                        <li>Форма обратной связи</li>
                    </ul>
                    <button class="btn btn-outline-primary d-flex mx-auto" data-toggle="modal" data-target="#service_modal">Хочу это</button>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 d-flex justify-content-between flex-column text-center mt-5 product-box-item">
                    <h3>Визитка</h3>
                    <p>от <span class="font-weight-bold">35 тыс.</span></p>
                    <ul>
                        <li>Две и более страниц</li>
                        <li>Все услуги</li>
                        <li>Полная информация о фирме</li>
                        <li>Форма обратной связи</li>
                    </ul>
                    <button class="btn btn-outline-primary d-flex mx-auto" data-toggle="modal" data-target="#service_modal">Хочу это</button>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 d-flex justify-content-between flex-column text-center mt-5 product-box-item">
                    <h3>Интернет магазин</h3>
                    <p>от <span class="font-weight-bold">80 тыс.</span></p>
                    <ul>
                        <li>Много страниц</li>
                        <li>Полная информация о товарах</li>
                        <li>Полная информация о фирме</li>
                        <li>Карточка товара</li>
                        <li>Корзина</li>
                        <li>Форма обратной связи</li>
                    </ul>
                    <button class="btn btn-outline-primary d-flex mx-auto" data-toggle="modal" data-target="#service_modal">Хочу это</button>
                </div>
            </div>
        </div>
    </section>

    <section class="accordion" id="homework">
        <div class="container products pt-6">

            <div class="card my-3">
                <div class="card-header p-0" id="heading_1">
                    <h2 class="mb-0">
                        <button class="btn btn-light btn-block text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapse_1" aria-expanded="true" aria-controls="collapseOne">
                            Урок 1
                        </button>
                    </h2>
                </div>
                <div id="collapse_1" class="collapse" aria-labelledby="heading_1" data-parent="#homework">
                    <div class="card-body">
                        <div class="row product-box justify-content-between pt-5 pb-5 homework-item">
                        <p><?= swapVars() ?></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card my-3">
                <div class="card-header p-0" id="heading_2">
                    <h2 class="mb-0">
                        <button class="btn btn-light btn-block text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapse_2" aria-expanded="true" aria-controls="collapseOne">
                            Урок 2
                        </button>
                    </h2>
                </div>
                <div id="collapse_2" class="collapse" aria-labelledby="heading_2" data-parent="#homework">
                    <div class="card-body">
                        <div class="row product-box justify-content-between pt-5 pb-5 homework-item">
                            <ol>
                                <li><?= randomActionsWithTwoRandomVars() ?></li>
                                <li><?php echo "\$a = rand(0, 15)<br>"; fifteenSwitches() ?></li>
                                <li><?= thirdQuestResolver() ?></li>
                                <li><?= fourthQuestResolver() ?></li>
                                <li><?= "date('Y') или (new \DateTime())->format('Y')" ?></li>
                                <li><?= sixthQuestResolver() ?></li>
                                <li><?= seventhQuestResolver() ?></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card my-3">
                <div class="card-header p-0" id="heading_3">
                    <h2 class="mb-0">
                        <button class="btn btn-light btn-block text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapse_3" aria-expanded="true" aria-controls="collapseOne">
                            Урок 3
                        </button>
                    </h2>
                </div>
                <div id="collapse_3" class="collapse" aria-labelledby="heading_3" data-parent="#homework">
                    <div class="card-body">
                        <div class="row product-box justify-content-between pt-5 pb-5 homework-item">
                            <ol>
                                <li><?= first() ?></li>
                                <li><?= second() ?></li>
                                <li><?= third($moscow, $spb, $ryazan, $regions) ?></li>
                                <li>makeAlias()</li>
                                <li><?= fifth("привет, коша") ?></li>
                                <li>sixth()</li>
                                <li><?= seventh() ?></li>
                                <li><?= eighth($moscow, $spb, $ryazan, $regions) ?></li>
                                <li>ninth($str)</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card my-3">
                <div class="card-header p-0" id="heading_4">
                    <h2 class="mb-0">
                        <button class="btn btn-light btn-block text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapse_4" aria-expanded="true" aria-controls="collapseOne">
                            Урок 4
                        </button>
                    </h2>
                </div>
                <div id="collapse_4" class="collapse" aria-labelledby="heading_4" data-parent="#homework">
                    <div class="card-body">
                        <div class="image-block d-flex flex-wrap justify-content-between text-center">
                            <?php foreach (getImagesByPath("../public/img/pizzes") as $pictureUrl) : ?>
                                <div class="col-md-4">
                                    <a data-fancybox="gallery" href="<?= $pictureUrl ?>" target="_blank" rel="noopener noreferrer">
                                        <img src="<?= $pictureUrl ?>" class="img-thumbnail" alt="collection-image" width="200" height="200">
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card my-3">
                <div class="card-header p-0" id="heading_5">
                    <h2 class="mb-0">
                        <button class="btn btn-light btn-block text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapse_5" aria-expanded="true" aria-controls="collapseOne">
                            Урок 5
                        </button>
                    </h2>
                </div>
                <div id="collapse_5" class="collapse" aria-labelledby="heading_5" data-parent="#homework">
                    <div class="card-body">
                        <div class="row mb-3">
                            <?php /*
                    <form id="image_loading_form" action="../controllers/5-lesson.php" method="post" class="col-6" enctype="multipart/form-data">
                        <div class="form-file">
                            <input id="load_images" name="images[]" type="file" multiple class="form-file-input">
                            <label class="form-file-label" for="load_images">
                                <span class="form-file-text">Choose files...</span>
                                <span class="form-file-button">Browse</span>
                            </label>
                        </div>
                    </form>
                    <div class="col-6">
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-outline-success text-left">Go</button>
                        </div>
                    </div>
                    */?>
                        </div>
                        <div class="row">
                            <div id="db_gallery_container" class="image-block d-flex flex-wrap justify-content-between text-center">
                                <?php foreach (getImages() as $key => $image) : ?>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <a data-fancybox="gallery" href="..<?= $image['path'] ?>/<?= $image['name'] ?>" rel="noopener noreferrer">
                                                <img onclick="increaseViewCount(<?= $image['id'] ?>)" src="..<?= $image['path'] ?>/<?= $image['name'] ?>" class="img-thumbnail" alt="collection-image" width="200" height="200">
                                            </a>
                                        </div>
                                        <div class="row">
                                    <span id="views_count_<?= $image['id'] ?>"
                                          class="badge rounded-pill bg-primary text-center w-50 mx-auto mb-3">
                                        <?= $image['views'] ?>
                                    </span>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card my-3">
                <div class="card-header p-0" id="heading_6">
                    <h2 class="mb-0">
                        <button class="btn btn-light btn-block text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapse_6" aria-expanded="true" aria-controls="collapseOne">
                            Урок 6
                        </button>
                    </h2>
                </div>
                <div id="collapse_6" class="collapse" aria-labelledby="heading_6" data-parent="#homework">
                    <div class="card-body">
                        <div class="row product-box homework-item">
                            <div class="image-block d-flex flex-wrap justify-content-between">
                                <div class="row border-dark border-bottom mb-3 text-center">
                                    <div class="col-6 form-action-item">
                                        <h5>Select action</h5>
                                        <form id="select_form" action="#" method="post" class="border-success border-right">
                                            <div class="form-group">
                                                <label for="num1" class="form-label w-50">
                                                    <input id="num1" class="form-control select-form text-center" name="num1" type="number" required>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="num2" class="form-label w-50">
                                                    <input id="num2" class="form-control select-form text-center" name="num2" type="number" required>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="select_action" class="form-label w-50">
                                                    <select class="form-select select-form" name="action" id="select_action">
                                                        <option value="+">Сложение</option>
                                                        <option value="-">Вычитание</option>
                                                        <option value="*">Умножение</option>
                                                        <option value="/">Деление</option>
                                                    </select>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <button form="select_form" class="btn btn-outline-success mb-2" type="submit">Do it</button>
                                            </div>
                                            <div class="form-group">
                                                <label for="select_result" class="form-label w-50">
                                                    <textarea id="select_result" name="select_result" class="form-control text-center" rows="1" disabled readonly></textarea>
                                                </label>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-6 form-action-item">
                                        <h5>Radio action</h5>
                                        <form id="radio_form" action="#" method="post" class="border-primary border-left">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label for="radio_num1" class="form-label w-50">
                                                        <input id="radio_num1" class="form-control text-center" name="num1" type="number" required>
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                    <label for="radio_num2" class="form-label w-50">
                                                        <input id="radio_num2" class="form-control text-center" name="num2" type="number" required>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="form-check">
                                                        <input id="sum" class="btn-check" name="action" value="+" type="radio" aria-label="..." required>
                                                        <label for="sum" class="btn btn-outline-primary w-50">+</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input id="subtract" class="btn-check" name="action" value="-" type="radio" aria-label="..." required>
                                                        <label for="subtract" class="btn btn-outline-primary w-50">-</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <button form="radio_form" class="btn btn-outline-primary mb-2" type="submit" disabled>I'm a robot!</button>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-check">
                                                        <input id="multiplication" class="btn-check" name="action" value="*" type="radio" aria-label="..." required>
                                                        <label for="multiplication" class="btn btn-outline-primary w-50">*</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input id="divide" class="btn-check" name="action" type="radio" value="/" aria-label="..." required>
                                                        <label for="divide" class="btn btn-outline-primary w-50">/</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label for="radio_result" class="form-label w-50">
                                                        <textarea id="radio_result" name="checkbox_result" class="form-control text-center" rows="1" disabled readonly></textarea>
                                                    </label>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="row border-info border-bottom mb-3">
                                    <h4 class="text-center">Отзывы</h4>
                                    <form id="reviews_form" action="#" method="post" class="row">
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="user_name" class="form-label">Name</label>
                                                <input id="user_name" class="form-control" name="user_name" type="text" placeholder="Boris" required>
                                            </div>
                                            <div class="mb-3">
                                                <label for="text" class="form-label">Text</label>
                                                <textarea id="text" name="text" class="form-control" rows="6" placeholder="Very nice..." required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button form="reviews_form" class="btn btn-outline-info" type="submit">Submit</button>
                                            </div>
                                        </div>
                                        <div class="col reviews-block">
                                            <div class="row">
                                                <div class="col-4">
                                                    <div id="reviews-list" class="list-group"></div>
                                                </div>
                                                <div class="col-8">
                                                    <div id="reviews-scrollspy" data-spy="scroll" data-target="#reviews-list" data-offset="0" class="reviews-scrollspy" tabindex="0"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="row text-center">
                                    <h4>Каталог</h4>
                                    <?php getCatalogSection() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card my-3">
                <div class="card-header p-0" id="heading_7">
                    <h2 class="mb-0">
                        <button class="btn btn-light btn-block text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapse_7" aria-expanded="true" aria-controls="collapseOne">
                            Урок 7
                        </button>
                    </h2>
                </div>
                <div id="collapse_7" class="collapse show" aria-labelledby="heading_7" data-parent="#homework">
                </div>
            </div>

        </div>
    </section>

    <div class="modal fade" id="service_modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Просто форма</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <form action="#" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="email" class="form-label">Email address</label>
                            <input type="email" class="form-control" id="email" aria-describedby="emailHelp">
                            <div id="emailHelp" class="form-text">Lorem ipsum dolor sit amet.</div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="form-label">Message</label>
                            <textarea class="form-control" rows="6" id="message"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-warning" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-outline-success">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="jquery-3.5.1.js" type="text/javascript"></script>
    <script src="../public/js/bootstrap-js.min.js" type="text/javascript"></script>
    <script src="../public/js/fontawesome.kit.js" type="text/javascript"></script>
    <script src="../public/js/jquery.fancybox.min.js" type="text/javascript"></script>
    <script src="../public/js/main.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
</body>
</html>
<?php

function getImagesByPath(string $path = "../public/img/pizzes") {
    return glob($path."/*.{jpg,png,jpeg}", GLOB_BRACE);
}

<?php

function tabs($count = 1) {
    $defaultString = "&nbsp;&nbsp;&nbsp;&nbsp";
    for ($i = 1; $i < $count; $i++) {
        $defaultString .= "&nbsp;&nbsp;&nbsp;&nbsp";
    }
    return $defaultString;
}

function swapVars() {
    $a = 11;
    $b = 12;

    list($a, $b) = [$b, $a];
    // $a=$a+$b;
    // $b=$a-$b;
    // $a=$a-$b;

    return "\$a = 11; <br> \$b = 12; <br><br> list($a, $b) = [$b, $a]";
}

// 1
function randomActionsWithTwoRandomVars() {
    $a = rand(-10, 10);
    $b = rand(-10, 10);

    $action = "+";

    if ($a >= 0 && $b >= 0) {
        $action = "-";
    }
    if ($a < 0 && $b < 0) {
        $action = "*";
    }
    // if (($a < 0 && $b > 0) || ($a > 0 && $b < 0)) {
    //     $action = "+";
    // }
    switch ($action) {
        case "-":
            $result = $a - $b;
            break;
        case "*":
            $result = $a * $b;
            break;
        default:
            $result = $a + $b;
            break;
    }

    return "\$a и \$b = rand(-10, 10) <br> \$a = $a <br> \$b = $b <br><br> \$a $action \$b = $result";
}

// 2
function fifteenSwitches() {
    $a = rand(0, 15);

    switch ($a) {
        case 0:
            echo $a++ . "<br>";
        case 1:
            echo $a++ . "<br>";
        case 2:
            echo $a++ . "<br>";
        case 3:
            echo $a++ . "<br>";
        case 4:
            echo $a++ . "<br>";
        case 5:
            echo $a++ . "<br>";
        case 6:
            echo $a++ . "<br>";
        case 7:
            echo $a++ . "<br>";
        case 8:
            echo $a++ . "<br>";
        case 9:
            echo $a++ . "<br>";
        case 10:
            echo $a++ . "<br>";
        case 11:
            echo $a++ . "<br>";
        case 12:
            echo $a++ . "<br>";
        case 13:
            echo $a++ . "<br>";
        case 14:
            echo $a++ . "<br>";
        default:
            echo $a;
            break;
    }
}

// 3
function sumNums($a, $b) {
    return $a + $b;
}

function subtract($a, $b) {
    return $a - $b;
}

function multiplication($a, $b) {
    return $a * $b;
}

function divide($a, $b) {
    if ($b == 0) {
        return "wtf?";
    }
    return $a / $b;
}

function thirdQuestResolver() {
    return '
    function sumNums($a, $b) { <br>
    ' . tabs() . ' return $a + $b; <br>
    } <br>
    <br>
    function subtract($a, $b) { <br>
    ' . tabs() . ' return $a - $b; <br>
    } <br>
    <br>
    function multiplication($a, $b) { <br>
    ' . tabs() . ' return $a * $b; <br>
    } <br>
    <br>
    function divide($a, $b) { <br>
    ' . tabs() . ' if ($b == 0) { <br>
    ' . tabs(2) . ' return "wtf?"; <br>
    ' . tabs() . ' } <br>
    ' . tabs() . ' return $a / $b; <br>
    } <br>
    ';
}

// 4
function mainMathActions(int $a, int $b, string $action) {
    switch ($action) {
        case "-":
            return subtract($a, $b);
        case "*":
            return multiplication($a, $b);
        case "/":
            return divide($a, $b);
        case "+":
            return sumNums($a, $b);
    default:
        return "Какая-то ошибка";
    }
}

function fourthQuestResolver() {
    return '
    switch ($action) { <br>
    ' . tabs() . ' case "-": <br>
        ' . tabs(2) . ' return subtract($a, $b); <br>
    ' . tabs() . ' case "*": <br>
        ' . tabs(2) . ' return multiplication($a, $b); <br>
    ' . tabs() . ' case "/": <br>
        ' . tabs(2) . ' return divide($a, $b); <br>
    ' . tabs() . ' case "+": <br>
        ' . tabs(2) . ' return sumNums($a, $b); <br>
    default: <br>
    ' . tabs() . ' return "Какая-то ошибка"; <br>
    }
    ';
}

// 6
function power($val, int $pow) {
    if ($pow < 0) {
        return "Error";
    }
    if ($pow != 0) {
        return $val * power($val, $pow - 1);
    }
    return 1;
}
power(2, 3);

function sixthQuestResolver() {
    return '
    function power($val, $pow) { <br>
    ' . tabs() . ' if ($pow <= 0) { <br>
        ' . tabs(2) . ' return "Error"; <br>
    ' . tabs() . ' } <br>
    ' . tabs() . ' if ($pow != 0) { <br>
        ' . tabs(2) . ' return $val * power($val, $pow - 1); <br>
    ' . tabs() . ' } <br>
    ' . tabs() . ' return 1; <br>
    } <br>
    power(2, 3);
    ';
}

// 7
function getDateWords() {
    $h = date('H');
    $m = date('i');

    if ($h == 1 || $h == 21) {
        $hours = "час";
    } elseif (($h >= 2 && $h <= 4) || ($h >= 22 && $h <= 24)) {
        $hours = "часа";
    } else {
        $hours = "часов";
    }
    if (($m % 10) == 1) {
        $minutes = "минута";
    } elseif ((($m % 10) >= 2) && (($m % 10) <= 4)) {
        $minutes = "минуты";
    } else {
        $minutes = "минут";
    }

    return "$h $hours $m $minutes";
}

function seventhQuestResolver() {
    return '
    $h = date("H"); <br>
    $m = date("i"); <br>
    <br>
    if ($h == 1 || $h == 21) { <br>
    ' . tabs() . ' $hours = "час"; <br>
    } elseif (($h >= 2 && $h <= 4) || ($h >= 22 && $h <= 24)) { <br>
    ' . tabs() . ' $hours = "часа"; <br>
    } else { <br>
    ' . tabs() . ' $hours = "часов"; <br>
    } <br>
    if (($m % 10) == 1) { <br>
    ' . tabs() . ' $minutes = "минута"; <br>
    } elseif ((($m % 10) >= 2) && (($m % 10) <= 4)) { <br>
    ' . tabs() . ' $minutes = "минуты"; <br>
    } else { <br>
    ' . tabs() . ' $minutes = "минут"; <br>
    } <br>
    <br>
    Итог: ' . getDateWords();
}
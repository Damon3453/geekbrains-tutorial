<?php

require_once 'MainController.php';

// 1
function first() {
    $a = 0;
    while ($a <= 100) {
        if ($a % 3 == 0) {
            echo "$a <br>";
        }
        $a++;
    }
}

// 2
function second() {
    $a = 0;
    echo "$a &mdash; это ноль <br>";
    do {
        $a++;
        $res = $a % 2 == 0 ? "$a &mdash; чётное число <br>" : "$a &mdash; нечётное число <br>";
        echo $res;
    } while ($a < 10);
}

// 3
function third($moscow, $spb, $ryazan, $regions) {
    foreach ($regions as $key => $region) {
        $lastElem = end($region);
        foreach ($region as $item) {
            // условия для вывода знаков препинания. не стал заморачиваться
            if ($item != $lastElem) {
                ${$key} .= "$item, ";
            }
            if ($item == $lastElem) {
                ${$key} .= "$item.";
            }
        }
    }

    return "$moscow <br> $spb <br> $ryazan";
}

// 4
// я уже делал это в своей практике, поэтому вставил готовое и немного не понял момент:
// мне кажется задание сложным для такого раннего этапа, видимо, оно должно выполняться как-то иначе.
// тогда я не понял как именно, поэтому оставляю свою старую наработку
function makeTranslit(string $str) {
    // FIXME: if $str is ENG return
    $converter = [
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    ];

    return strtr($str, $converter);
}
function makeAlias(string $str) {
    $str = strtolower(makeTranslit($str));
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    $str = trim($str, "-");

    return $str;
}

// 5
function fifth(string $str) {
    $res = "";
    for ($i = 0; $i < mb_strlen($str); $i++) {
        $item = mb_substr($str, $i, 1);
        if ($item == " ") {
            $item = "_";
        }
        $res .= $item;
    }

    return $res;
}

// 6
function sixth() {
    return getHeaderMenuItems();
}

// 7
function seventh() {
    for ($i = 0; print $i, $i++ < 9;) {}
}

// 8
function eighth($moscow, $spb, $ryazan, $regions) {
    foreach ($regions as $key => $region) {
        $lastElem = end($region);
        foreach ($region as $item) {
            // $item[0] -- не проходит кодировку
            if (mb_substr($item, 0, 1) == "К") {
                if ($item != $lastElem) {
                    ${$key} .= "$item, ";
                }
                if ($item == $lastElem) {
                    ${$key} .= "$item.";
                }
            }
        }
    }

    return "$moscow <br> $spb <br> $ryazan";
}

// 9
function ninth(string $str) {
    return makeAlias(fifth($str));
}
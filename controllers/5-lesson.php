<?php

require_once "../env.php";
// ---------
// Блок, проверяющий какой метод вызывается с фронта
if (isset($_GET['query']) && $_GET['query'] == true) {
    increaseViewCount($_GET['image_id']);
}
if (isset($_GET['remake']) && $_GET['remake'] == true) {
    getImages(true);
}
// ----------

// if (isset($_POST['submit']) && !empty($_FILES['images'])) {
//     loadFile($_FILES['images']);
// } else {
//     echo "pls select file(s)";
// }
/**
 * Попытка загружать файлы в БД. без фреймворка времени много уходит. так что @deprecated
 *
 * @link https://www.php.net/manual/ru/features.file-upload.php
 * @param $images
 */
function loadFile($images) {
    // $root = $_SERVER['DOCUMENT_ROOT'];
    // for ($i = 0; $i < count($images['name']); $i++) {
    //     if ($images['size'][$i] > pow(1024, 2)) {
    //         echo "Too large";
    //     }
    //     try {
    //         move_uploaded_file($images['tmp_name'][$i], "../uploads/img/{$images['tmp_name'][$i]}");
    //     } catch (RuntimeException $e) {
    //         echo $e->getMessage();
    //     }
    // }

    header('Content-Type: text/plain; charset=utf-8');

    for ($i = 0; $i < count($images['name']); $i++) {
        try {
            // Undefined | Multiple Files | $_FILES Corruption Attack
            // If this request falls under any of them, treat it invalid.
            if (
                !isset($images['error'][$i]) ||
                is_array($images['error'][$i])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $images['error'] value.
            switch ($images['error'][$i]) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            // You should also check filesize here.
            if ($images['size'][$i] > pow(1024, 2)) {
                throw new RuntimeException('Exceeded filesize limit.');
            }

            // DO NOT TRUST $images['mime'] VALUE !!
            // Check MIME Type by yourself.
            $fileInfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = array_search(
                    $fileInfo->file($images['tmp_name'][$i]),
                    array(
                        'jpg' => 'image/jpeg',
                        'png' => 'image/png',
                        'gif' => 'image/gif',
                    ),
                    true
                )) {
                throw new RuntimeException('Invalid file format.');
            }

            // You should name it uniquely.
            // DO NOT USE $images['name'] WITHOUT ANY VALIDATION !!
            // On this example, obtain safe unique name from its binary data.
            $filename = sprintf('../uploads/img/%s.%s', md5($images['name'][$i]), $ext);
            if (!move_uploaded_file($images['tmp_name'][$i], $filename)) {
                throw new RuntimeException('Failed to move uploaded file.');
            }
            // loadToDb($filename, "/uploads/img");
            echo 'File is uploaded successfully.';

        } catch (RuntimeException $e) {
            echo $e->getMessage();
        }
    }
}

/**
 * Получить контент галереи. Параметр определяет откуда произошёл вызов метода.
 * Если один счётчик views стал больше другого -- значение true и перерисовка блока
 *
 * @param false $remake
 *
 * @return array|bool
 */
function getImages(bool $remake = false) {
    $link = baseConnect();

    $sql = 'select * from images ORDER BY `views` DESC';
    $result = $link->query($sql);
    $arr = [];

    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            $arr[] = $row;
        }
    } else {
        echo "0 results";
    }

    $link->close();

    if ($remake) {
        echo json_encode([
            'message' => 'Success',
            'data' => $arr,
            'status' => 201
        ], 0, 201);

        return true;
    }

    return $arr;
}

/**
 * Увеличивает счётчик просмотров
 *
 * @param $id
 *
 * @return void|bool
 */
function increaseViewCount($id): bool
{
    $link = baseConnect();

    $select = 'select `views` from `images` where `id` = ' . $id;
    $currentViews = mysqli_fetch_assoc($link->query($select))['views'];

    $maxInDB = (int) mysqli_fetch_assoc($link->query('select * from images ORDER BY `views` DESC limit 1'))['views'];
    $currentViews++;

    $update = "update `images` SET `views` = " . $currentViews . " WHERE `id` = " . $id;
    $link->query($update);

    // Если один счётчик views стал больше другого -- отправляем флаг (другой статус)
    if ($currentViews > $maxInDB) {
        $link->close();

        echo json_encode([
            'message' => 'Success',
            'data' => ['count' => $currentViews],
            'status' => 201
        ], 0, 201);

        return true;
    }

    $link->close();

    echo json_encode([
        'message' => 'Success',
        'data' => ['count' => $currentViews],
        'status' => 200
    ], 0, 200);
}

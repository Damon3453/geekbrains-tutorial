<?php

require_once '1-2-lessons.php';
require_once 'APIBaseController.php';
require_once '../env.php';
require_once '../Requests/BaseInputRequest.php';

if (isset($_POST['select_form']) && $_POST['select_form'] == true) {
    makeFormAction($_POST['data']);
}
if (isset($_POST['radio_form']) && $_POST['radio_form'] == true) {
    makeFormAction($_POST['data']);
}
if (isset($_POST['add_review']) && $_POST['add_review'] == true) {
    addReview($_POST['data']);
}
if (isset($_POST['get_all']) && $_POST['get_all'] == true) {
    getAllReviews();
}
if (isset($_POST['get_products']) && $_POST['get_products'] == true) {
    getProductInfo($_POST['product_id']);
}

function makeFormAction($data) {
    foreach ($data as $item) {
        if ($item['value'] == "") {
            echo sendError('Error', ['result' => 'Заполните поля!'], 422);

            return false;
        }
        ${$item['name']} = $item['value'];
    }
    echo sendSuccess(['result' => mainMathActions($num1, $num2, (string) $action)], 'Success');

    return true;
}

function addReview($data) {
    $link = baseConnect();

    try {
        $link->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
        $link->autocommit(FALSE);

        $name = clearMysqlInputs($link, $data['name']);
        $addUser = "INSERT INTO users (name) VALUES ('" . $name . "');";
        $link->query($addUser);

        $getUserId = "SELECT id FROM users WHERE name = '" . $name . "' order by id desc limit 1;";
        $userId = mysqli_fetch_assoc($link->query($getUserId))['id'];

        $text = clearMysqlInputs($link, $data['text']);
        $addReview = "INSERT INTO reviews (text, user_id) VALUES  ('" .$text . "', " . (int) $userId . ");";
        $link->query($addReview);

        $link->commit();

    } catch (RuntimeException $e) {
        $link->rollback();
        echo json_encode($e->getMessage());

        return false;
    }

    $link->close();

    return getAllReviews();
}

function getAllReviews() {
    $link = baseConnect();

    $sql = 'select * from reviews left join users on users.id = reviews.user_id order by created_at desc';
    $result = $link->query($sql);
    $arr = [];
    $key = 0;

    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            $arr[] = $row;
            $arr[$key]['created_at'] = date('d-m-Y H:i', strtotime($arr[$key]['created_at']));
            $arr[$key]['updated_at'] = date('d-m-Y H:i', strtotime($arr[$key]['updated_at']));
            $key++;
        }
    } else {
        echo "0 results";
    }

    $link->close();

    echo sendSuccess($arr, 'Ok');

    return true;
}

function getProducts() {
    $link = baseConnect();

    $sql = 'select * from products';
    $result = $link->query($sql);
    $arr = [];

    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            $arr[] = $row;
        }
    } else {
        echo "0 results";
    }

    $link->close();

    return $arr;
}

function getProductInfo($productId) {
    $link = baseConnect();
    $id = clearMysqlInputs($link, $productId);

    $sql = 'select * from products where id = ' . $id;
    $result = mysqli_fetch_assoc($link->query($sql));

    $link->close();

    echo json_encode($result);

    return true;
}
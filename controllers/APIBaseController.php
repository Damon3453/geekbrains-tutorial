<?php

/**
 * @param array $data
 * @param string $message
 * @param int $status
 *
 * @return false|string
 */
function sendSuccess(array $data, string $message = '', int $status = 200) {
    http_response_code($status);

    return json_encode([
        'success' => true,
        'data' => $data,
        'message' => $message,
        'status' => $status
    ]);
}

/**
 * @param string $message
 * @param null|array $data
 * @param int $status
 *
 * @return false|string
 */
function sendError(string $message = '', $data = null, int $status = 422) {
    http_response_code($status);

    return json_encode([
        'success' => false,
        'data' => $data,
        'message' => $message,
        'status' => $status
    ]);
}
<?php

// class MainController
// {}

function getInfoBlockContent() {
    return [
        'brand' => [
            'img_1' => 'public/img/infoblock_1.png',
            'heading' => 'Раскручиваем бренд в интернете',
            'text' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque deleniti, incidunt perspiciatis similique sit maxime.'
        ],
        'comfort' => [
            'img_1' => 'public/img/infoblock_2-1.png',
            'img_2' => 'public/img/infoblock_2-2.png',
            'heading' => 'Добавляем технологий в ваш дом во имя комфорта',
            'text' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque deleniti, incidunt perspiciatis similique sit maxime.'
        ],
        'needs' => [
            'img_1' => 'public/img/infoblock_3.png',
            'heading' => 'Создаём цифровые продукты, учитывая ваши приоритетные нужды',
            'text' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque deleniti, incidunt perspiciatis similique sit maxime.'
        ],
    ];
}

function getHeaderMenuItems() {
    return [
        '
        <li>
            <a href="/">
                <img src="../../../public/img/logo_main_page.svg" alt="Главная">
            </a>
        </li>
        ',
        '
        <li>
            <a href="/landing" class="btn btn-outline-primary" target="_blank">Домашка по курсу PHP</a>
        </li>
        ',
        '
        <li>
            <a href="../../../contacts.php">
                <img src="../../../public/img/logo_contacts.svg" alt="Контакты">
            </a>
        </li>
        '
    ];
}

function getCatalogSection() {
    include "../views/catalog.php";
}
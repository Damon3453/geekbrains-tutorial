<?php
    require_once "required_params.php";
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>

    <link rel="shortcut icon" href="public/img/fav.ico">

    <link rel="stylesheet" href="public/css/bootstrap5.min.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/media.css">
</head>
<body>
<div class="main-content-block index-page-gradient">

    <div class="container top-box">

        <?php
            include_once 'views/includes/layouts/header.php';
        ?>

        <div class="main-banner mb-6">
            <h1 class="main-heading"><?= $mainHeading ?></h1>
            <p class="main-heading__p"><?= $mainHeadingP ?></p>
            <?php
                // include_once 'views/includes/music-block.php';
            ?>
            <a href="#our-projects" class="main-banner__button">See our projects</a>
            <p>Кривой якорь
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-hand-index" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M6.75 1a.75.75 0 0 0-.75.75V9a.5.5 0 0 1-1 0v-.89l-1.003.2a.5.5 0 0 0-.399.546l.345 3.105a1.5 1.5 0 0 0 .243.666l1.433 2.15a.5.5 0 0 0 .416.223h6.385a.5.5 0 0 0 .434-.252l1.395-2.442a2.5 2.5 0 0 0 .317-.991l.272-2.715a1 1 0 0 0-.995-1.1H13.5v1a.5.5 0 0 1-1 0V7.154a4.208 4.208 0 0 0-.2-.26c-.187-.222-.368-.383-.486-.43-.124-.05-.392-.063-.708-.039a4.844 4.844 0 0 0-.106.01V8a.5.5 0 0 1-1 0V5.986c0-.167-.073-.272-.15-.314a1.657 1.657 0 0 0-.448-.182c-.179-.035-.5-.04-.816-.027l-.086.004V8a.5.5 0 0 1-1 0V1.75A.75.75 0 0 0 6.75 1zM8.5 4.466V1.75a1.75 1.75 0 0 0-3.5 0v5.34l-1.199.24a1.5 1.5 0 0 0-1.197 1.636l.345 3.106a2.5 2.5 0 0 0 .405 1.11l1.433 2.15A1.5 1.5 0 0 0 6.035 16h6.385a1.5 1.5 0 0 0 1.302-.756l1.395-2.441a3.5 3.5 0 0 0 .444-1.389l.272-2.715a2 2 0 0 0-1.99-2.199h-.582a5.184 5.184 0 0 0-.195-.248c-.191-.229-.51-.568-.88-.716-.364-.146-.846-.132-1.158-.108l-.132.012a1.26 1.26 0 0 0-.56-.642 2.634 2.634 0 0 0-.738-.288c-.31-.062-.739-.058-1.05-.046l-.048.002zm2.094 2.025z"/>
                </svg>
            </p>
        </div>
    </div>
</div>

<div class="container additive-content d-flex flex-column">
    <h3 class="additive-content__heading">Что мы делаем, чтобы помочь наши клиентам расти в цифровом пространстве?</h3>

    <div class="additive-content__info-blocks">
        <?php foreach ($info as $infoBlock): ?>
        <div class="info-block">
            <div class="info-block__picture">
                <img src="<?= $infoBlock['img_1'] ?>" alt="mini-pic">
                <?php
                    array_key_exists('img_2', $infoBlock) ? "<img src='{$infoBlock['img_2']}' alt='mini-pic'>" : ""
                ?>
            </div>
            <div class="info-block__heading">
                <h4><?= $infoBlock['heading'] ?></h4>
            </div>
            <div class="info-block__p">
                <p><?= $infoBlock['text'] ?></p>
                <a href="#" class="disabled info-block__href">Learn more
                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-arrow-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M10.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L12.793 8l-2.647-2.646a.5.5 0 0 1 0-.708z"/>
                        <path fill-rule="evenodd" d="M2 8a.5.5 0 0 1 .5-.5H13a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8z"/>
                    </svg>
                </a>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>

<section id="our-projects">
    <div class="container projects">
        <div class="our-project-block text-center">
            <h2 class="our-project-block__heading">Наши проекты</h2>
            <p class="our-project-block__p">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia optio consequatur officiis ex dolorum autem nam quidem ut? Explicabo, dolore nisi! Libero, maxime? Quas libero dicta nobis. Suscipit ab ullam facere, deleniti unde ipsum dolorem ipsam possimus dolorum, sequi commodi!</p>
        </div>

        <div class="project-picture-block">
            <img src="public/img/project-picture.png" class="project-picture-block__image" alt="project-pic-1">
            <p class="project__text">Smart Home Installation</p>
        </div>

        <div class="mobile-apps">
            <div class="project-picture-block">
                <img src="public/img/sparklite.png" class="project-picture-block__image mobile-apps__image" alt="project-pic-2">
                <p class="mobile-apps__project__text">Sparklite App</p>
            </div>
            <div class="project-picture-block">
                <img src="public/img/car-rapetition.png" class="project-picture-block__image mobile-apps__image" alt="project-pic-3">
                <p class="mobile-apps__project__text">Car-Rapetition App</p>
            </div>
        </div>
    </div>
</section>

<?php
    include_once 'views/includes/layouts/footer.php';
    include_once 'views/includes/popup-consult-form.php';
?>

<script src="public/js/jquery.min.js"></script>
<script src="public/js/bootstrap-js.min.js"></script>
<script src="public/js/fontawesome.kit.js"></script>
<script src="public/js/main.js"></script>
</body>
</html>
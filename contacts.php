<?php
    require_once "required_params.php";
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Контакты</title>

    <link rel="shortcut icon" href="public/img/fav.ico">

    <link rel="stylesheet" href="public/css/bootstrap5.min.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/media.css">
</head>
<body>
    <div class="main-content-block">

        <div class="container">
            <?php
                include_once 'views/includes/layouts/header.php';
            ?>
        </div>

        <div class="main__contacts-banner-area">
            <div class="main__contacts-text">
                <h2 class="text-center main__contacts__heading-second">Contact Us</h2>
                <p class="main__contacts__p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
            </div>
        </div>

        <div class="container">
            <div class="contacts__content">
                <div class="contacts__data-block">
                    <div class="contacts__picture">
                        <img src="public/img/php_logo.png" alt="contacts-picture">
                    </div>
                    <div class="contacts__address">
                        <h4 class="contacts__data-block__heading">Address</h4>
                        <p class="contacts__data-block__p">Id convallis placerat sit sed duis id amet volutpat quam a, pharetra.</p>
                    </div>
                    <div class="contacts__phone">
                        <h4 class="contacts__data-block__heading">Phone</h4>
                        <p class="contacts__data-block__p"><i class="fas fa-phone"></i>8 800 555 35 35</p>
                        <p class="contacts__data-block__p"><i class="fas fa-phone"></i>36 12 10</p>
                    </div>
                    <div class="contacts__services">
                        <h4 class="contacts__data-block__heading">Online service</h4>
                        <p class="contacts__data-block__p">
                            <i class="fas fa-globe"></i>
                            <a href="https://pepperfm.ru" target="_blank" rel="noopener noreferrer">Моя студия</a>
                        </p>
                        <p class="contacts__data-block__p">
                            <i class="far fa-envelope"></i>
                            <a href="mailto:damon3453@yandex.ru" target="_blank" rel="noopener noreferrer">Damon3453@yandex.ru</a>
                        </p>
                    </div>
                </div>

                <div class="contacts__form-block">
                    <h4 class="form-heading">Send us message</h4>
                    <form action="#" method="post" class="contact-form">
                        <div class="custom-form-group">
                            <label for="name">Full Name:</label>
                            <input type="text" name="name" class="form-control form-inputs" id="name" placeholder="Your name">
                        </div>
                        <div class="custom-form-group">
                            <label for="email">Email:</label>
                            <input type="email" name="mail" class="form-control form-inputs" placeholder="E-mail address" id="email" required>
                        </div>
                        <div class="custom-form-group">
                            <label for="message">Message:</label>
                            <textarea name="message" class="form-control form-inputs" cols="30" rows="7" placeholder="Enter message" id="message" required></textarea>
                        </div>
                        <button type="submit" class="form-button">Submit</button>
                    </form>
                </div>                
            </div>
        </div>
    </div>

    <section id="map"></section>

    <?php
        include_once 'views/includes/layouts/footer.php';
        include_once 'views/includes/popup-consult-form.php';
    ?>

    <script src="public/js/jquery.min.js"></script>
    <script src="public/js/map.js"></script>
    <script src="public/js/bootstrap-js.min.js"></script>
    <script src="public/js/fontawesome.kit.js"></script>
    <script src="public/js/main.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
</body>
</html>

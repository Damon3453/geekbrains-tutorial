$(function() {
	var myMap;
	ymaps.ready(init);

	function init () {
		myMap = new ymaps.Map('map', {
			center: [
				51.660781, 39.200269
			],
			zoom: 16,
			width: '100%'
		});

		myMap.geoObjects.add(new ymaps.Placemark([51.660781, 39.200269], {
			preset: 'islands#yellowDotIcon'
		}));
	}
});
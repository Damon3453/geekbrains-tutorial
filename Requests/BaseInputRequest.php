<?php

function clearMysqlInputs($link, $var) {
    return mysqli_real_escape_string($link, strip_tags($var));
}
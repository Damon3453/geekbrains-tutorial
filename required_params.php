<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'controllers/MainController.php';

$mainHeading = 'New Automation Tool for Your';
$mainHeadingP = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam porro nisi et fugiat culpa impedit blanditiis, officia natus, id aperiam cumque facere modi? Ducimus consequatur pariatur vero similique minima dolores?';
$currentYear = date('Y');

$info = getInfoBlockContent();
$headerMenuItems = getHeaderMenuItems();

$moscow = "Московская область: <br>";
$spb = "Ленинградская область: <br>";
$ryazan = "Рязанская область: <br>";
$regions = [
    'moscow' => ['Москва', 'Зеленоград', 'Клин'],
    'spb' => ['Санкт-Петербург', 'Всеволожск', 'Павловск', 'Кронштадт'],
    'ryazan' => ['Рязань', 'Скопин', 'Касимов'],
];

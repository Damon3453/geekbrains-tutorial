<?php

namespace App;

class DigitalProduct extends Product
{
    /**
     * @return float
     */
    public function calculateSum(): float
    {
        return $this->cost;
    }
}

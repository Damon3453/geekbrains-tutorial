<?php

namespace App;

/**
 * Class WeightProduct
 * @package App
 *
 * @property int $weight
 */
class WeightProduct extends Product
{
    /** @var int $weight в граммах */
    private int $weight;

    public function __construct(int $cost, int $weight)
    {
        $this->weight = $weight;
        parent::__construct($cost);
    }

    /**
     * @return float
     */
    public function calculateSum(): float
    {
        return $this->cost * ($this->weight / 1000);
    }
}

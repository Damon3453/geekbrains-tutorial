<?php

namespace App;

/**
 * Class RealProduct
 * @package App
 *
 * @property int $count
 */
class RealProduct extends Product
{
    /** @var int $count */
    private int $count;

    public function __construct(int $cost, int $count)
    {
        $this->cost = $count;
        parent::__construct($cost);
    }

    /**
     * @return float
     */
    public function calculateSum(): float
    {
        return $this->cost * $this->count;
    }
}

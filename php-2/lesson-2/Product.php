<?php
declare(strict_types=1);

namespace App;

use App\Interfaces\ProductInterface;

/**
 * Class Product
 * @package App
 *
 * @property int $cost
 */
abstract class Product implements ProductInterface
{
    /** @var int $cost */
    protected int $cost;

    /**
     * Product constructor.
     *
     * @param int $cost
     */
    public function __construct(int $cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return float
     */
    abstract public function calculateSum(): float;
}

<?php

namespace App\Interfaces;

interface ProductInterface
{
    public function calculateSum(): float;
}

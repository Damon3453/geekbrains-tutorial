<?php
declare(strict_types=1);

/**
 * Class Drink
 * @package landing/php-2/lesson-1
 *
 * @property string $name
 * @property string $description
 * @property string $price
 * @property string $volume
 */
class Drink extends Product
{
    /**
     * Объём
     * @var string
     */
    private string $volume;

    /**
     * Drink constructor.
     *
     * @param string $name
     * @param string $description
     * @param string $price
     * @param string $volume
     */
    public function __construct(string $name = '', string $description = '', string $price = '', string $volume = '')
    {
        $this->volume = $volume;
        parent::__construct($name, $description, $price);
    }

    /**
     * @return string
     */
    public function getVolume() : string
    {
        return $this->volume;
    }

    /**
     * @param string $volume
     */
    public function setVolume(string $volume) : void
    {
        $this->volume = $volume;
    }
}
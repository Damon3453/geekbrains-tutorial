<?php
declare(strict_types=1);

/**
 * Class Product
 * @package landing/php-2/lesson-1
 *
 * @property string $name
 * @property string $description
 * @property string $price
 */
class Product
{
    /**
     * @var string
     */
    protected string $name;
    /**
     * @var string
     */
    protected string $description;
    /**
     * @var string
     */
    protected string $price;

    // protected array $params;

    /**
     * Product constructor.
     *
     * @param string $name
     * @param string $description
     * @param string $price
     */
    public function __construct(string $name = '', string $description = '', string $price = '')
    {
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
    }
}
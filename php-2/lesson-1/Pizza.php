<?php
declare(strict_types=1);

/**
 * Class Pizza
 * @package landing/php-2/lesson-1
 *
 * @property string $name
 * @property string $description
 * @property string $price
 * @property int $diameter
 */
class Pizza extends Product
{
    /**
     * @var int
     */
    private int $diameter;

    /**
     * Pizza constructor.
     *
     * @param string $name
     * @param string $description
     * @param string $price
     * @param int $diameter
     */
    public function __construct(string $name = '', string $description = '', string $price = '', int $diameter = 30)
    {
        $this->diameter = $diameter;
        parent::__construct($name, $description, $price);
    }

    /**
     * @return string
     */
    public function getPrice() : string
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice(string $price) : void
    {
        $this->price = $price;
    }
}
<?php

class Gallery
{
    private static array $config = [
        'dns' => 'mysql:dbname=geekbrains_tutorial;host=127.0.0.1',
        'user' => 'root',
        'pwd' => 'root'
    ];
    private PDO $link;

    public function __construct()
    {
        $this->link = new PDO(
            self::$config['dns'],
            self::$config['user'],
            self::$config['pwd'],
        );
    }

    /**
     * Получить контент галереи. Параметр определяет откуда произошёл вызов метода.
     * Если один счётчик views стал больше другого -- значение true и перерисовка блока
     *
     * @param false $remake
     *
     * @return array|\http\Client\Response|string|void
     */
    public function getImages(bool $remake = false)
    {
        try {
            $sql = 'select * from images ORDER BY `views` DESC';
            $arr = $this->link->query($sql)->fetchAll(PDO::FETCH_ASSOC);

            if ($remake) {
                return json_encode([
                    'message' => 'Success',
                    'data' => $arr,
                    'status' => 201
                ], 0, 201);
            }

            return $arr;
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * Увеличивает счётчик просмотров
     *
     * @param $id
     *
     * @return false|\http\Client\Response|string|void
     */
    public function increaseViewCount($id)
    {
        try {
            $currentViews = (int) $this->link
                ->query("select `views` from `images` where `id` = $id")
                ->fetch(PDO::FETCH_ASSOC)['views'];
            $maxInDB = (int) $this->link
                ->query('select * from images ORDER BY `views` DESC limit 1')
                ->fetch()['views'];
            $currentViews++;
            $this->link->query("update `images` SET `views` = $currentViews WHERE `id` = $id");

            // Если один счётчик views стал больше другого -- отправляем флаг (другой статус)
            if ($currentViews > $maxInDB) {
                return json_encode([
                    'message' => 'Success',
                    'data' => ['count' => $currentViews],
                    'status' => 201
                ], 0, 201);
            }

            return json_encode([
                'message' => 'Success',
                'data' => ['count' => $currentViews],
                'status' => 200
            ], 0, 200);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }
}

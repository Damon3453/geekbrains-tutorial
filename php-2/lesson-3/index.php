<?php
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    require_once "Gallery.php";
    require '../../vendor/autoload.php';
    $loader = new Twig\Loader\FilesystemLoader('../../views/_partials');
    $twig = new Twig\Environment($loader);
?><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Landing</title>

    <link rel="stylesheet" href="../../public/css/bootstrap5.min.css">
    <link rel="stylesheet" href="../../public/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <section class="accordion" id="homework">
        <div class="container products pt-6">

            <div class="card my-3">
                <div class="card-header p-0" id="heading_5">
                    <h2 class="mb-0">
                        <button class="btn btn-light btn-block text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapse_5" aria-expanded="true" aria-controls="collapseOne">
                            Урок 3
                        </button>
                    </h2>
                </div>
                <div id="collapse_5" class="collapse show" aria-labelledby="heading_5" data-parent="#homework">
                    <div class="card-body">
                        <div class="row">
                            <div id="db_gallery_container" class="image-block d-flex flex-wrap justify-content-between text-center">
                                <?= $twig->render('gallery.twig', ['images' => ((new Gallery())->getImages())]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <script src="jquery-3.5.1.js" type="text/javascript"></script>
    <script src="../../public/js/bootstrap-js.min.js" type="text/javascript"></script>
    <script src="../../public/js/fontawesome.kit.js" type="text/javascript"></script>
    <script src="../../public/js/jquery.fancybox.min.js" type="text/javascript"></script>
    <script src="../../public/js/main.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
</body>
</html>

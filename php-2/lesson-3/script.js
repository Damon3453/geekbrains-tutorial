function increaseViewCount(id) {
  $.ajax({
    url: `galleryQueryHandler.php`,
    method: 'get',
    data: {
      query: true,
      image_id: id
    },
    dataType: 'json',
    success: function (response) {
      $(`#views_count_${id}`).html(response.data.count)
      remakeGalleryHtml()
    },
    error: function (error) {
      throw error
    }
  })
}

function remakeGalleryHtml() {
  $.ajax({
    url: `galleryQueryHandler.php`,
    method: 'get',
    data: {
      remake: true,
    },
    dataType: 'json',
    success: function (response) {
      $('#db_gallery_container').html('')
      response.data.forEach(item => {
        $('#db_gallery_container').append(`
          <div class="col-md-4">
            <div class="row">
              <a data-fancybox="gallery" href="../../${item.path}/${item.name}" rel="noopener noreferrer">
                <img onclick="increaseViewCount(${item.id})" src="../../${item.path}/${item.name}" class="img-thumbnail" alt="collection-image" width="200" height="200">
              </a>
            </div>
            <div class="row">
              <span
                id="views_count_${item.id}"
                class="badge rounded-pill bg-primary text-center w-50 mx-auto mb-3"
                >
                  ${item.views}
              </span>
            </div>
          </div>
        `)
      })
    },
    error: function (error) {
      throw error
    }
  })
}
